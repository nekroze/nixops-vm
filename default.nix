{config, ...}:
{
  imports = [
    ./guest.nix
    ./host.nix
    ./persistence.nix
  ];
}

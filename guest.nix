{config, lib, pkgs, ...}:
with lib;

let
  cfg = config.modules.vmg;
  mkLibvirtBridgeDeviceXML = interface: ''
    <interface type='bridge'>
      <model type='virtio'/>
      <source bridge='${interface}'/>
      ${optionalString cfg.openVswitch ''<virtualport type='openvswitch'></virtualport>''}
    </interface>
  '';
  mkLibvirtFsMap = tag: target: ''
    <filesystem type='mount' accessmode='mapped'>
      <source dir='${target.host}'/>
      <target dir='${tag}'/>
    </filesystem>
  '';
  mkVboxFsMap = tag: target: {
    hostPath = target.host;
  };
  mkMountFsMap = tag: target: {
    name = target.guest;
    value = {
      fsType = if cfg.libvirt then "9p" else "vboxsf";
      device = tag;
      options = ["rw"] ++ (if cfg.libvirt then ["trans=virtio" "version=9p2000.L" "nobootwait" "_netdev"] else []);
    };
  };
  mkFakeFsMap = tag: target: {
    name = target.guest;
    value = {
      options = ["bind"];
      device = "/var/lib/mock/${tag}";
    };
  };
in {
  imports = [
    ./persistence.nix
    <nixpkgs/nixos/modules/profiles/qemu-guest.nix>
  ];
  options = {
    modules.vmg = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable the nix managed vm guest config.";
      };
      libvirt = mkOption {
        type = types.bool;
        default = true;
        description = "Use the libvirt VM backend instead of virtualbox.";
      };
      libvirtDevicesXML = mkOption {
        type = types.lines;
        default = "";
        description = "XML for extra devices to assign to the libvirt VM.";
      };
      libvirtDomainXML = mkOption {
        type = types.lines;
        default = "";
        description = "XML for extra libvirt config.";
      };
      memory = mkOption {
        type = types.int;
        default = 512;
        description = "Memory in MB to assign to the VM.";
      };
      memoryBalloon = mkOption {
        type = types.bool;
        default = true;
        description = "Use the libvirt virtio balloon for thin provisioning memory.";
      };
      appArmor = mkOption {
        type = types.bool;
        default = false;
        description = "Use app armor security contexts for VM.";
      };
      cores = mkOption {
        type = types.int;
        default = 1;
        description = "Number of virtual CPU cores to assign to the libvirt VM.";
      };
      hostCPU = mkOption {
        type = types.bool;
        default = true;
        description = "Use the host's cpu module in the VM.";
      };
      headless = mkOption {
        type = types.bool;
        default = true;
        description = "Make the VM headless.";
      };
      bridges = mkOption {
        type = types.listOf types.str;
        default = [];
        description = "Bridge interfaces to add as additional devices.";
      };
      additionalNetworks = mkOption {
        type = types.listOf types.str;
        default = [];
        description = "Additional libvirt networks to connect to.";
      };
      clock = mkOption {
        type = types.str;
        default = "utc";
        example = "localtime";
        description = "Libvirt clock offset to use for the VM's hardware clock.";
      };
      defaultNetwork = mkOption {
        type = types.str;
        default = "default";
        description = "Libvirt network for nixops connection to this vm.";
      };
      openVswitch = mkOption {
        type = types.bool;
        default = false;
        description = "Use openVswitch for bridges.";
      };
      persistence = mkOption {
        type = types.bool;
        default = false;
        description = "Add persistant storage on /var.";
      };
      persistenceSize = mkOption {
        type = types.int;
        default = 1;
        description = "Size of persistant storage in GB.";
      };
      persistenceSparse = mkOption {
        type = types.bool;
        default = false;
        description = "Sparse persistance.";
      };
      fsMap = mkOption {
        type = types.bool;
        default = true;
        description = "Enable host to guest fs maps.";
      };
      fsMaps = let
        mapOptions = { name, ... }: {
          options = {
            host = mkOption {
              type = types.str;
              description = "Path on the host to share with the guest.";
            };
            guest = mkOption {
              type = types.str;
              description = "Path on the guest to mount the share.";
            };
          };
        };
      in mkOption {
        type = types.attrsOf (types.submodule mapOptions);
        default = {};
        example = literalExample ''
        {
          "media" = {
            host = "/mnt/media";
            guest = "/media";
          };
        };
        '';
        description = "Host to guest fs maps.";
      };
    };
  };

  config = mkIf cfg.enable {
    deployment = {
      targetEnv = if cfg.libvirt then "libvirtd" else "virtualbox";
      virtualbox = mkIf (!cfg.libvirt) {
        headless = true;
        memorySize = cfg.memory;
        sharedFolders = mapAttrs mkVboxFsMap (if cfg.fsMap then cfg.fsMaps else {});
      };
      libvirtd = mkIf cfg.libvirt {
        headless = true;
        memorySize = cfg.memory;
        vcpu = cfg.cores;
        networks = [ cfg.defaultNetwork ] ++ cfg.additionalNetworks;
        extraDomainXML = cfg.libvirtDomainXML + ''
          ${optionalString cfg.hostCPU "<cpu mode='host-model'></cpu>"}
          <clock offset='${cfg.clock}'/>
        '';
        extraDevicesXML = cfg.libvirtDevicesXML
          + (concatStringsSep "\n" (map mkLibvirtBridgeDeviceXML cfg.bridges))
          + (concatStringsSep "\n" (if cfg.fsMap then (attrValues (mapAttrs mkLibvirtFsMap cfg.fsMaps)) else []))
          + ''
          ${if cfg.headless then ''
            <console type='pty'>
              <target type='serial' port='0'/>
            </console>
          '' else ''
            <graphics type='spice' port='5900' autoport='yes' listen='127.0.0.1'>
              <listen type='address' address='127.0.0.1'/>
            </graphics>
            <video>
              <model type='cirrus' vram='16384' heads='1' primary='yes'/>
              <alias name='video0'/>
              <address type='pci' domain='0x0000' bus='0x00' slot='0x04' function='0x0'/>
            </video>
          ''}
          ${if cfg.memoryBalloon then ''
            <memballoon model='virtio'/>
          '' else ""}
          ${if cfg.appArmor then ''
            <seclabel type='dynamic' model='apparmor'/>
          '' else ""}
        '';
      };
    };
    modules.persistence = mkIf cfg.persistence {
      enable = true;
      size = cfg.persistenceSize;
      sparse = cfg.persistenceSparse;
    };
    services.nscd.enable = false;
    services.ntp.enable = mkDefault false;
    fileSystems = listToAttrs (attrValues (mapAttrs (if cfg.fsMap then mkMountFsMap else mkFakeFsMap) cfg.fsMaps));
    boot = mkIf (cfg.libvirt) {
      kernelParams = if cfg.headless then [ "console=ttyS0,115200" ] else [];
      loader.grub.extraConfig = if cfg.headless then "serial; terminal_input serial; terminal_output serial" else "";
    };
  };
}

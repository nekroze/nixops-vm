{config, pkgs, lib, resources, ...}:
with lib;

let
  cfg = config.modules.persistence;
  mkVolume = name: target: {
    xml = ''
      <volume type='block'>
        <name>${name}</name>
        <capacity unit='G'>${toString target.modules.persistence.size}</capacity>
        ${optionalString target.modules.persistence.sparse ''<allocation unit='M'>100</allocation>''}
      </volume>
    '';
    pool = target.modules.persistence.pool;
    size = "${toString target.modules.persistence.size}G";
  };
  guestsWithPersistence = (filterAttrs (n: m: m.modules.persistence.enable && !m.modules.persistence.host) resources.machines);
in {
  imports = [
    ./host.nix
    ./guest.nix
  ];
  options = {
    modules.persistence = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable persistence on this vm via a disk mounted to /var.";
      };
      host = mkOption {
        type = types.bool;
        default = false;
        description = "Enable the host side of the persistence drives.";
      };
      pool = mkOption {
        type = types.str;
        default = "persistence";
        description = "Storage pool on which to create the peristance volume.";
      };
      size = mkOption {
        type = types.int;
        default = 1;
        description = "Size of peristant storage in GB.";
      };
      sparse = mkOption {
        type = types.bool;
        default = false;
        description = "Attempt to create a sparse volume, allocating on 100M.";
      };
    };
  };

  config = mkIf (cfg.enable || cfg.host) {
    modules.vmh = mkIf config.modules.vmh.enable {
      libvirtVolumes = mapAttrs mkVolume guestsWithPersistence;
    };
    modules.vmg = mkIf (config.modules.vmg.enable && (!config.modules.vmh.enable) ) {
      libvirtDevicesXML = ''
        <disk type='volume' device='disk'>
          <source pool='persistence' volume='${config.networking.hostName}'/>
          <target dev='vda' bus='virtio'/>
        </disk>
      '';
    };
    systemd.services = mkIf (config.modules.vmg.enable && (!config.modules.vmh.enable) ) {
      autoresize-persistence = {
        wantedBy = [ "default.target" "multi-user.target" ];
        path = with pkgs; [ e2fsprogs ];
        requires = [ "var-lib.mount" ];
        after = [ "var-lib.mount" ];
        script = ''
          if [ -e /dev/vda ]; then
            resize2fs /dev/vda
          fi
        '';
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = true;
        };
      };
    };
    fileSystems = mkIf (config.modules.vmg.enable && (!config.modules.vmh.enable) ) {
      "/var/lib" = {
        device = "/dev/vda";
        options = [ "rw" "relatime" ];
        fsType = "ext4";
        autoFormat = true;
        autoResize = true;
        neededForBoot = true;
      };
    };
  };
}

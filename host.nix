{config, lib, pkgs, ...}:
with lib;
let
  cfg = config.modules.vmh;
  mkBridge = name: target: {
    interfaces = target.interfaces;
  };
  bridgeOptions = { name, ... }: {
    options = {
      interfaces = mkOption {
        type = types.listOf types.string;
        default = [];
        description = "Interfaces to attach to this vswitch.";
      };
    };
  };
  libvirtDefineOptions = { name, ... }: {
    options = {
      xml = mkOption {
        type = types.lines;
        description = "XML to define.";
      };
    };
  };
  libvirtVolumeOptions = { name, ... }: {
    options = {
      pool = mkOption {
        type = types.str;
        default = "persistence";
        description = "Pool to target.";
      };
      size = mkOption {
        type = types.str;
        description = "Size of the volume.";
      };
    };
  };
  mkLibvirtPoolUnit = name: target: {
    name = "libvirt-pool-${name}";
    value = {
      after = [ "libvirtd.service" ];
      wantedBy = [ "default.target" "multi-user.target" ];
      path = with pkgs; [ libvirt ];
      script = ''
        virsh pool-define ${pkgs.writeText "libvirt-pool-${name}.xml" target.xml}
        virsh pool-autostart ${name}
        if [[ -z "$(virsh pool-info ${name} | grep State | grep running)" ]]; then
          virsh pool-start --build ${name} || virsh pool-start ${name}
        fi
      '';
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
      };
    };
  };
  mkLibvirtNetUnit = name: target: {
    name = "libvirt-net-${name}";
    value = {
      after = [ "libvirtd.service" ];
      wantedBy = [ "default.target" "multi-user.target" ];
      path = with pkgs; [ libvirt ];
      script = ''
        virsh net-define ${pkgs.writeText "libvirt-net-${name}.xml" target.xml}
        virsh net-autostart ${name}
        if [[ -z "$(virsh net-info ${name} | grep Active | grep yes)" ]]; then
          virsh net-start ${name}
        fi
      '';
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
      };
    };
  };
  mkLibvirtVolUnit = name: target: {
    name = "libvirt-vol-${name}";
    value = let
      create = ''
        if [[ -z "$(virsh vol-list ${target.pool} | grep ${name})" ]]; then
          virsh vol-create ${target.pool} ${pkgs.writeText "libvirt-vol-${name}.xml" target.xml}
        fi
      '';
    in {
      after = [ "libvirt-pool-${target.pool}.service" ];
      wantedBy = [ "default.target" "multi-user.target" ];
      path = with pkgs; [ libvirt devicemapper gawk ];
      script = create;
      reload = ''
         if [[ "$(virsh vol-list ${target.pool} --details | grep ${name} | awk '{print int($4)}')G" != "${target.size}" ]]; then
           lvresize -L ${target.size} $(virsh vol-list ${target.pool} | grep ${name} | awk '{print $2}')
           virsh blockresize $(virsh list | grep ${name} | awk '{print $2}') $(virsh vol-list ${target.pool} | grep ${name} | awk '{print $2}') ${target.size}
           virsh vol-delete ${name} --pool ${target.pool}
         fi
      '' + create;
      reloadIfChanged = true;
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
      };
    };
  };
in {
  options = {
    modules.vmh = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable the nix managed libvirt module.";
      };
      nestedIntel = mkOption {
        type = types.bool;
        default = false;
        description = "Nested intel kvm.";
      };
      bridges = mkOption {
        default = {};
        type = types.attrsOf (types.submodule bridgeOptions);
        example = literalExample ''
        {
          "br0" = {
            interfaces = [ "eno1" ];
          };
        };
        '';
        description = "Attribute set of virtual networks to create bridges for.";
      };
      libvirtNetworks = mkOption {
        default = {};
        type = types.attrsOf (types.submodule libvirtDefineOptions);
        example = literalExample ''
        {
          "internal" = {
            xml = builtins.readFile ./net-internal.xml
          };
        };
        '';
        description = "Attribute set of libvirt networks to define.";
      };
      libvirtPools = mkOption {
        default = {};
        type = types.attrsOf (types.submodule libvirtDefineOptions);
        example = literalExample ''
        {
          "persistence" = {
            xml = builtins.readFile ./pool-persistence.xml
          };
        };
        '';
        description = "Attribute set of libvirt storage pools to define.";
      };
      libvirtVolumes = mkOption {
        default = {};
        type = types.attrsOf (types.submodule [ libvirtDefineOptions libvirtVolumeOptions ]);
        example = literalExample ''
        {
          "git" = {
            xml = builtins.readFile ./pool-persistence.xml
            pool = "default";
          };
        };
        '';
        description = "Attribute set of libvirt storage volumes to define.";
      };
      libvirtDomains = mkOption {
        default = {};
        type = types.attrsOf (types.submodule libvirtDefineOptions);
        example = literalExample ''
        {
          "pfSense" = {
            xml = builtins.readFile ./dom-pfsense.xml
          };
        };
        '';
        description = "Attribute set of libvirt domains to define.";
      };
    };
  };

  config = mkIf cfg.enable {
    networking.bridges = mapAttrs mkBridge cfg.bridges;
    boot.kernelModules = [ "tun" "virtio" "kvm-intel" "dm_snapshot" "dm_zero" ];
    boot.kernelParams = optional cfg.nestedIntel "kvm-intel.nested=1";
    hardware.enableKSM = true;
    virtualisation.libvirtd = {
      enable = true;
      enableKVM = true;
      extraConfig = ''
        dynamic_ownership = 0
      '';
    };
    networking.firewall.checkReversePath = false;
    modules.persistence = {
      enable = false;
      host = true;
    };
    systemd.services = mkMerge [
      {
        libvirt-images-dir = {
          before = [ "libvirtd.service" ];
          wantedBy = [ "default.target" "multi-user.target" ];
          script = "mkdir -p /var/lib/libvirt/images";
          serviceConfig = {
            Type = "oneshot";
          };
        };
      }
      (mapAttrs' mkLibvirtNetUnit cfg.libvirtNetworks)
      (mapAttrs' mkLibvirtPoolUnit cfg.libvirtPools)
      (mapAttrs' mkLibvirtVolUnit cfg.libvirtVolumes)
    ];
    environment.systemPackages = [ pkgs.libvirt ] ++ (optional config.services.xserver.enable pkgs.virt-viewer);
  };
}
